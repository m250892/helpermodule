package com.example.manoj.loginhelper;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import com.example.manoj.loginhelper.fragments.LoginFragment;
import com.example.manoj.loginhelper.helper.BaseLoginHelperActivity;

public class LoginActivity extends BaseLoginHelperActivity {

    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        frameLayout = (FrameLayout) findViewById(R.id.fragment_container);
        loadInitialFragment();
    }

    private void loadInitialFragment() {
        addFragment(LoginFragment.newInstance());
    }

    private void addFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
    }
}
