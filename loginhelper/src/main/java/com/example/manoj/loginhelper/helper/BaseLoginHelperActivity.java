package com.example.manoj.loginhelper.helper;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.manoj.loginhelper.models.IPermissionsCallback;
import com.facebook.FacebookSdk;

import java.util.HashMap;

/**
 * Created by manoj on 12/02/16.
 */
public class BaseLoginHelperActivity extends AppCompatActivity {

    private HashMap<Integer, IPermissionsCallback> requestCodeToCallbackMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeFbSdk();
        initializeLoginHelpers();
    }

    private void initializeLoginHelpers() {
        GPlusLoginHelper.getInstance().setFragmentActivity(this);
        FbLoginHelper.getInstance().setFragmentActivity(this);
    }

    private void initializeFbSdk() {
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("manoj", "on Activity result load called in Login Activity");

        if (requestCode == GPlusLoginHelper.GPLUS_SIGN_IN) {
            GPlusLoginHelper.getInstance().onActivityResult(requestCode, resultCode, data);
        } else {
            FbLoginHelper.getInstance().onActivityResult(requestCode, resultCode, data);
        }
    }

    public boolean checkPermissionGranted(String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) ==
                PackageManager.PERMISSION_GRANTED;
    }

    public void unregisterForPermissionRequest(int requestCode) {
        requestCodeToCallbackMap.remove(requestCode);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        IPermissionsCallback permissionsCallback = requestCodeToCallbackMap.get(requestCode);
        if (permissionsCallback != null) {
            unregisterForPermissionRequest(requestCode);
            permissionsCallback.onPermissionRequestResult(requestCode, grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void requestPermission(int requestCode, IPermissionsCallback permissionsCallback) {
        String permission = null;
        switch (requestCode) {
            case IPermissionsCallback.GET_ACCOUNTS:
                permission = Manifest.permission.GET_ACCOUNTS;
                break;
            default:
                new RuntimeException("Permission not find in callbacks");
        }

        if (checkPermissionGranted(permission)) {
            permissionsCallback.onPermissionRequestResult(requestCode, true);
        } else {
            requestCodeToCallbackMap.put(requestCode, permissionsCallback);
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }

}
