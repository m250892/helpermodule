package com.example.manoj.loginhelper.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.manoj.loginhelper.R;
import com.example.manoj.loginhelper.helper.BaseSignUpWithEmailHelperFragment;

/**
 * Created by manoj on 13/02/16.
 */
public class SignUpFragment extends BaseSignUpWithEmailHelperFragment implements View.OnClickListener {

    private EditText inputEmail, inputPassword, inputName;
    private TextInputLayout inputLayoutEmail, inputLayoutPassword;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = inflater.inflate(getLayoutResourceId(), container, false);
        initViews(baseView);
        return baseView;
    }

    public int getLayoutResourceId() {
        return R.layout.signup_fragment_layout;
    }

    protected void initViews(View baseView) {
        baseView.findViewById(R.id.fb_login_button).setOnClickListener(this);
        baseView.findViewById(R.id.google_login_button).setOnClickListener(this);

        inputLayoutEmail = (TextInputLayout) baseView.findViewById(R.id.input_layout_email);
        inputLayoutPassword = (TextInputLayout) baseView.findViewById(R.id.input_layout_password);
        inputEmail = (EditText) baseView.findViewById(R.id.input_email);
        inputPassword = (EditText) baseView.findViewById(R.id.input_password);

        baseView.findViewById(R.id.btn_signup).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (R.id.fb_login_button == id) {
            doLoginWithFb();
        } else if (R.id.google_login_button == id) {
            doLoginWithGPlus();
        } else if (R.id.btn_signup == id) {
            onSignUpButtonClick();
        }
    }

    private void onSignUpButtonClick() {
        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();
        onSignUpWithEmail(email, password, null);
    }

    @Override
    public void setEmailError(String error) {
        inputLayoutEmail.setError(error);
    }

    @Override
    public void setPasswordError(String error) {
        inputLayoutPassword.setError(error);
    }

    @Override
    public void disableEmailError() {
        inputLayoutEmail.setErrorEnabled(false);
    }

    @Override
    public void disablePasswordError() {
        inputLayoutPassword.setErrorEnabled(false);
    }


}
