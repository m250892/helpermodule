package com.example.manoj.loginhelper.models.fbloginresponse;

import android.location.Location;

public class Work
{
    private Position position;

    private String end_date;

    private Location location;

    private Employer employer;

    private String start_date;

    public Position getPosition ()
    {
        return position;
    }

    public void setPosition (Position position)
    {
        this.position = position;
    }

    public String getEnd_date ()
    {
        return end_date;
    }

    public void setEnd_date (String end_date)
    {
        this.end_date = end_date;
    }

    public Location getLocation ()
    {
        return location;
    }

    public void setLocation (Location location)
    {
        this.location = location;
    }

    public Employer getEmployer ()
    {
        return employer;
    }

    public void setEmployer (Employer employer)
    {
        this.employer = employer;
    }

    public String getStart_date ()
    {
        return start_date;
    }

    public void setStart_date (String start_date)
    {
        this.start_date = start_date;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [position = "+position+", end_date = "+end_date+", location = "+location+", employer = "+employer+", start_date = "+start_date+"]";
    }
}