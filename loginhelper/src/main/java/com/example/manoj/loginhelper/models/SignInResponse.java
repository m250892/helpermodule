package com.example.manoj.loginhelper.models;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manoj on 07/12/15.
 */
public class SignInResponse extends Model implements Cloneable {
    @SerializedName("login_type")
    private String loginType;
    @SerializedName("login_id")
    private String loginId;
    @SerializedName("id")
    private String id;
    private String profileUrl;
    private String name;
    private String email;
    private String gender;
    @SerializedName("image_url")
    private String imageUrl;
    @SerializedName("min_age")
    private int minAge;
    private String dob;
    private boolean verified;
    private String mobile_number;
    @SerializedName("occupation")
    private String collageName;
    private String profilePicUrl;

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        if (null != gender && gender.length() > 1) {
            gender = gender.substring(0, 1).toUpperCase() + gender.substring(1, gender.length()).toLowerCase();
        }
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String profilePicUrl) {
        this.imageUrl = profilePicUrl;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getGenderAgeString() {
        return getGender() + ", " + minAge + " years";
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getProfilePicUrl() {
        if (null == profilePicUrl) {
            profilePicUrl = "https://graph.facebook.com/" + getId() + "/picture?type=large";
        }
        return imageUrl;
    }

    public String getPhoneNumber() {
        return mobile_number;
    }

    public void setPhoneNumber(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getCollageName() {
        return collageName;
    }

    public void setCollageName(String collage_name) {
        this.collageName = collage_name;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public class OrganizationInfo {
        private boolean primary;
        private String title;
        private String name;
        private String startDate;
        private String endDate;
        private String type;
    }

    public JSONObject getJsonObject() {
        Gson gson = new Gson();
        try {
            JSONObject jsonObject = new JSONObject(gson.toJson(this));
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Object clone() throws CloneNotSupportedException {
        SignInResponse cloned = (SignInResponse) super.clone();
        return cloned;
    }
}
