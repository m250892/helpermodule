package com.example.manoj.utilshelper.exeptions;

import com.example.manoj.utilshelper.rest.RequestError;

/**
 * Created by manoj on 13/02/16.
 */
public class NetworkException extends Exception {

    private String errorMessage;
    private int httpStatusCode;
    private String messageFromServer;

    private NetworkException(Throwable cause, int httpStatusCode, String errorMessage,
                             String messageFromServer) {
        super(errorMessage, cause);
        this.errorMessage = errorMessage;
        this.httpStatusCode = httpStatusCode;
        this.messageFromServer = messageFromServer;
    }

    @Override
    public String getMessage() {
        return errorMessage;
    }

    public String getMessageFromServer() {
        return messageFromServer;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public static class NetworkExceptionBuilder {

        private String errorMessage;
        private int httpStatusCode = RequestError.REQUEST_RESPONSE_NOT_OK;
        private String messageFromServer;
        private Throwable cause;

        public NetworkExceptionBuilder() {
        }

        public NetworkExceptionBuilder errorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
            return this;
        }

        public NetworkExceptionBuilder httpStatusCode(int httpStatusCode) {
            this.httpStatusCode = httpStatusCode;
            return this;
        }

        public NetworkExceptionBuilder messageFromServer(String messageFromServer) {
            this.messageFromServer = messageFromServer;
            return this;
        }

        public NetworkExceptionBuilder cause(Throwable th) {
            this.cause = th;
            return this;
        }

        public NetworkException createNetworkException() {
            return new NetworkException(cause, httpStatusCode, errorMessage, messageFromServer);
        }
    }
}