package com.example.manoj.loginhelper.models.gplusloginresponsemodel;

public class Image {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ClassPojo [url = " + url + "]";
    }
}

		