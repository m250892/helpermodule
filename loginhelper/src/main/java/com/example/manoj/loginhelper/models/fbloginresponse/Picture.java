package com.example.manoj.loginhelper.models.fbloginresponse;

public class Picture {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getUrl() {
        if (data != null) {
            return data.getUrl();
        }
        return null;
    }

    @Override
    public String toString() {
        return "ClassPojo [data = " + data + "]";
    }
}
