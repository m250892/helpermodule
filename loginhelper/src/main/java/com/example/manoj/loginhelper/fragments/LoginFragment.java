package com.example.manoj.loginhelper.fragments;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.manoj.loginhelper.R;
import com.example.manoj.loginhelper.helper.BaseUsernameLoginHelperFragment;
import com.example.manoj.loginhelper.models.SignInRequestCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseUsernameLoginHelperFragment implements View.OnClickListener, SignInRequestCallback {
    private EditText inputEmail, inputPassword;
    private TextInputLayout inputLayoutEmail, inputLayoutPassword;

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View baseView = inflater.inflate(R.layout.fragment_login, container, false);
        initViews(baseView);
        return baseView;
    }

    protected void initViews(View baseView) {
        baseView.findViewById(R.id.fb_login_button).setOnClickListener(this);
        baseView.findViewById(R.id.google_login_button).setOnClickListener(this);

        inputLayoutEmail = (TextInputLayout) baseView.findViewById(R.id.input_layout_email);
        inputLayoutPassword = (TextInputLayout) baseView.findViewById(R.id.input_layout_password);
        inputEmail = (EditText) baseView.findViewById(R.id.input_email);
        inputPassword = (EditText) baseView.findViewById(R.id.input_password);
        baseView.findViewById(R.id.btn_login).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.fb_login_button) {
            doLoginWithFb();
        } else if (id == R.id.google_login_button) {
            doLoginWithGPlus();
        } else if (id == R.id.btn_login) {
            onLoginButtonClick();
        }
    }

    protected void onLoginButtonClick() {
        String username = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();
        onLoginWithUsernameAndPassword(username, password);
    }

    @Override
    public void setEmailError(String error) {
        inputLayoutEmail.setError(error);
    }

    @Override
    public void setPasswordError(String error) {
        inputLayoutPassword.setError(error);
    }

    @Override
    public void disableEmailError() {
        inputLayoutEmail.setErrorEnabled(false);
    }

    @Override
    public void disablePasswordError() {
        inputLayoutPassword.setErrorEnabled(false);
    }
}
