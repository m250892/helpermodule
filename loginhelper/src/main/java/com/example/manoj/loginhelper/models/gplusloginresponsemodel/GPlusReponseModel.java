package com.example.manoj.loginhelper.models.gplusloginresponsemodel;

import android.media.Image;

public class GPlusReponseModel {
    private Organizations[] organizations;

    private Image image;

    private String isPlusUser;

    private String url;

    private AgeRange ageRange;

    private String id;

    private Cover cover;

    private String verified;

    private Name name;

    private PlacesLived[] placesLived;

    private String gender;

    private String circledByCount;

    private String language;

    private String displayName;

    private String objectType;

    public Organizations[] getOrganizations() {
        return organizations;
    }

    public void setOrganizations(Organizations[] organizations) {
        this.organizations = organizations;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getIsPlusUser() {
        return isPlusUser;
    }

    public void setIsPlusUser(String isPlusUser) {
        this.isPlusUser = isPlusUser;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public AgeRange getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(AgeRange ageRange) {
        this.ageRange = ageRange;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Cover getCover() {
        return cover;
    }

    public void setCover(Cover cover) {
        this.cover = cover;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public PlacesLived[] getPlacesLived() {
        return placesLived;
    }

    public void setPlacesLived(PlacesLived[] placesLived) {
        this.placesLived = placesLived;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCircledByCount() {
        return circledByCount;
    }

    public void setCircledByCount(String circledByCount) {
        this.circledByCount = circledByCount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    @Override
    public String toString() {
        return "ClassPojo [organizations = " + organizations + ", image = " + image + ", isPlusUser = " + isPlusUser + ", url = " + url + ", ageRange = " + ageRange + ", id = " + id + ", cover = " + cover + ", verified = " + verified + ", name = " + name + ", placesLived = " + placesLived + ", gender = " + gender + ", circledByCount = " + circledByCount + ", language = " + language + ", displayName = " + displayName + ", objectType = " + objectType + "]";
    }
}