package com.example.manoj.loginhelper.models;

public interface IPermissionsCallback {
    int GET_ACCOUNTS = 1;
    int GET_LOCATION = 2;

    void onPermissionRequestResult(int requestCode, boolean result);
}
