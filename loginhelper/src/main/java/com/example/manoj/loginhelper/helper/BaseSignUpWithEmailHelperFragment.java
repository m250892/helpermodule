package com.example.manoj.loginhelper.helper;

import com.example.manoj.loginhelper.LoginUtils;
import com.example.manoj.loginhelper.R;

import org.json.JSONObject;

/**
 * Created by manoj on 13/02/16.
 */
public abstract class BaseSignUpWithEmailHelperFragment extends BaseFbAndGPlusLoginHelperFragment {

    public void onSignUpWithEmail(String email, String password, JSONObject data) {
        boolean isError = false;
        if (!LoginUtils.validateEmail(email)) {
            isError = true;
            setEmailError(getResources().getString(R.string.error_email_not_valid));
        } else {
            disableEmailError();
        }

        if (!validatePassword(password)) {
            isError = true;
            setPasswordError(getResources().getString(R.string.error_password_not_valid));
        } else {
            disablePasswordError();
        }

        if (!isError) {
            doSignUpWithEmail();
        }
    }

    public void doSignUpWithEmail() {
        hideKeyboard();
        showToastMessage("OK! I'm performing SignUp.");
    }

    public abstract void setEmailError(String error);

    public abstract void setPasswordError(String error);

    public abstract void disableEmailError();

    public abstract void disablePasswordError();

}
