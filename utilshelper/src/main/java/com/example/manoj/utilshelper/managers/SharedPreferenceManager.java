package com.example.manoj.utilshelper.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.manoj.utilshelper.core.SharedPrefsUtils;

public class SharedPreferenceManager {

    static SharedPreferenceManager singleton = null;
    private static final String PREF_NAME = "package_name";
    static SharedPreferences preferences;

    static SharedPreferences.Editor editor;

    protected SharedPreferenceManager(Context context) {
        preferences = SharedPrefsUtils.getSharedPrefs(PREF_NAME, context);
        editor = preferences.edit();
    }

    private static SharedPreferenceManager with(Context context) {
        if (singleton == null) {
            singleton = new Builder(context).build();
        }
        return singleton;
    }

    /**
     * Builder class
     */
    private static class Builder {

        private final Context context;

        public Builder(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.context = context.getApplicationContext();
        }

        /**
         * Method that creates an instance of Prefs
         *
         * @return an instance of Prefs
         */
        public SharedPreferenceManager build() {
            return new SharedPreferenceManager(context);
        }
    }
}