package com.example.manoj.loginhelper.helper;

import com.example.manoj.loginhelper.LoginUtils;
import com.example.manoj.loginhelper.R;

/**
 * Created by manoj on 12/02/16.
 */
public abstract class BaseUsernameLoginHelperFragment extends BaseFbAndGPlusLoginHelperFragment {


    public void onLoginWithUsernameAndPassword(String username, String password) {
        boolean isError = false;
        if (!LoginUtils.validateEmail(username)) {
            isError = true;
            setEmailError(getResources().getString(R.string.error_email_not_valid));
        } else {
            disableEmailError();
        }

        if (!validatePassword(password)) {
            isError = true;
            setPasswordError(getResources().getString(R.string.error_password_not_valid));
        } else {
            disablePasswordError();
        }

        if (!isError) {
            doLoginWithUserName();
        }
    }


    public void doLoginWithUserName() {
        hideKeyboard();
        showToastMessage("OK! I'm performing login.");
    }


    public abstract void setEmailError(String error);

    public abstract void setPasswordError(String error);

    public abstract void disableEmailError();

    public abstract void disablePasswordError();

}
