package com.example.manoj.loginhelper.models.fbloginresponse;

public class AgeRange {
    private int min;

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    @Override
    public String toString() {
        return "ClassPojo [min = " + min + "]";
    }
}
