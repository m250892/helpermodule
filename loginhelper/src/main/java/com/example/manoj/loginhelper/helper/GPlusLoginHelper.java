package com.example.manoj.loginhelper.helper;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.manoj.loginhelper.LoginConstants;
import com.example.manoj.loginhelper.models.SignInRequestCallback;
import com.example.manoj.loginhelper.models.SignInResponse;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;

/**
 * Created by manoj on 07/12/15.
 */
public class GPlusLoginHelper implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private static GPlusLoginHelper gPlusLoginHelper;
    private Context mConstext;
    private FragmentActivity fragmentActivity;

    public boolean mResolvingError = false;
    public static final int GPLUS_SIGN_IN = 101;
    public static final int REQUEST_RESOLVE_ERROR = 1002;

    private SignInRequestCallback signInRequestCallback;

    private GPlusLoginHelper() {
    }

    public void setFragmentActivity(FragmentActivity fragmentActivity) {
        this.mConstext = fragmentActivity.getBaseContext();
        this.fragmentActivity = fragmentActivity;
        buildGoogleApiClient();
    }

    public static GPlusLoginHelper getInstance() {
        if (gPlusLoginHelper == null) {
            gPlusLoginHelper = new GPlusLoginHelper();
        }
        return gPlusLoginHelper;
    }

    protected synchronized void buildGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(mConstext)
                .enableAutoManage(fragmentActivity, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GPLUS_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("manoj", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            //Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d("manoj", "email : " + acct.getEmail() + ", Name:  " + acct.getDisplayName());

            fetchGPlusProfileDetail();
        } else {
            Log.d("manoj", "handleSignInResult false, signed out");
            // Signed out, show unauthenticated UI.
        }
    }

    public void signIn(SignInRequestCallback signInRequestCallback) {
        this.signInRequestCallback = signInRequestCallback;
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startGoogleLoginActivity();
        } else {
            connectGoogleApiClient();
        }
    }

    public void startGoogleLoginActivity() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        fragmentActivity.startActivityForResult(signInIntent, GPLUS_SIGN_IN);
    }

    private void fetchGPlusProfileDetail() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            if (mGoogleApiClient.hasConnectedApi(Plus.API)) {
                Log.d("manoj", "fetchGPlusProfileDetail called");
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                /*Gson gson = new Gson();
                GPlusReponseModel gPlusReponseModel = gson.fromJson(person.toString(), GPlusReponseModel.class);*/

                SignInResponse signInResponse = new SignInResponse();
                signInResponse.setEmail(email);
                signInResponse.setLoginType(LoginConstants.GPLUS_LOGIN);
                if (person != null) {
                    signInResponse.setLoginId(person.getId());
                    signInResponse.setName(person.getDisplayName());
                    signInResponse.setGender(person.getGender() == 0 ? "male" : "female");
                    signInResponse.setDob(person.getBirthday());
                    if (person.getAgeRange() != null) {
                        signInResponse.setMinAge(person.getAgeRange().getMin());
                    }
                    signInResponse.setVerified(person.hasVerified());
                    signInResponse.setProfileUrl(person.getUrl());
                    signInResponse.setImageUrl(person.getImage().getUrl());
                }
                if (signInRequestCallback != null) {
                    signInRequestCallback.onSignInSuccess(LoginConstants.FB_LOGIN, signInResponse);
                    signInRequestCallback = null;
                }
            } else {
                Log.d("manoj", "G-Plus not connected");
            }
        } else {
            Log.d("manoj", "G-Client not connected");
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d("manoj", "GPlusHelper onConnected called");
        startGoogleLoginActivity();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("manoj", "GPlusHelper onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.d("manoj", "onConnectionFailed called : " + result.toString());
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(fragmentActivity, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                connectGoogleApiClient();
            }
        }
    }

    public boolean connectGoogleApiClient() {
        if (!mGoogleApiClient.isConnecting() &&
                !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            return false;
        }
        return true;
    }

    public void disconnectGoogleApiClient() {
        if (mGoogleApiClient.isConnected()) {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            }
            mGoogleApiClient.disconnect();
        }
    }

    public void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });
    }

    private ResultCallback<People.LoadPeopleResult> userFriendsCallback = new ResultCallback<People.LoadPeopleResult>() {
        @Override
        public void onResult(People.LoadPeopleResult peopleData) {
            if (peopleData.getStatus().getStatusCode() == CommonStatusCodes.SUCCESS) {
                PersonBuffer personBuffer = peopleData.getPersonBuffer();
                try {
                    int count = personBuffer.getCount();
                    for (int i = 0; i < count; i++) {
                        Log.d("manoj", "Display name: " + personBuffer.get(i).getDisplayName());
                    }
                } finally {
                    personBuffer.release();
                }
            } else {
                Log.e("manoj", "Error requesting visible circles: " + peopleData.getStatus());
            }
        }
    };

    public void fetchUserFriendsDetail() {
        Plus.PeopleApi.loadVisible(mGoogleApiClient, null)
                .setResultCallback(userFriendsCallback);
    }

    public void unregisterCallback() {
        signInRequestCallback = null;
    }
}
