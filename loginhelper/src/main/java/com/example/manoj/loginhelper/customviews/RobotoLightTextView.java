package com.example.manoj.loginhelper.customviews;

import android.content.Context;
import android.util.AttributeSet;

public class RobotoLightTextView extends CustomTextView {

    public static final String FONT_NAME = "fonts/Roboto-Light.ttf";

    public RobotoLightTextView(Context context) {
        super(context);
    }

    public RobotoLightTextView(Context context, AttributeSet attr) {
        super(context, attr);
    }

    public RobotoLightTextView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
    }

    @Override
    protected String getFontName() {
        return FONT_NAME;
    }
}
