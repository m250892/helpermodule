package com.example.manoj.loginhelper.models.fbloginresponse;

public class Education {
    private School school;

    private Year year;

    private String type;

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Year getYear() {
        return year;
    }

    public void setYear(Year year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ClassPojo [school = " + school + ", year = " + year + ", type = " + type + "]";
    }
}
