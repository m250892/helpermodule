package com.example.manoj.loginhelper.models.fbloginresponse;


import java.util.ArrayList;

public class FbSignInResponseModel {

    private Picture picture;

    private String id;

    private String birthday;

    private String email;

    private boolean verified;

    private String link;

    private String name;

    private String gender;

    private ArrayList<Work> work;

    private ArrayList<Education> education;

    private AgeRange age_range;


    public ArrayList<Work> getWork() {
        return work;
    }

    public void setWork(ArrayList<Work> work) {
        this.work = work;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ArrayList<Education> getEducation() {
        return education;
    }

    public void setEducation(ArrayList<Education> education) {
        this.education = education;
    }

    public AgeRange getAgeRange() {
        return age_range;
    }

    public int getMinAge() {
        if (getAgeRange() != null) {
            return getAgeRange().getMin();
        }
        return 0;
    }

    public void setAge_range(AgeRange age_range) {
        this.age_range = age_range;
    }

    @Override
    public String toString() {
        return "ClassPojo [work = " + work + ", picture = " + picture + ", id = " + id + ", birthday = " + birthday + ", email = " + email + ", verified = " + verified + ", link = " + link + ", name = " + name + ", gender = " + gender + ", education = " + education + ", age_range = " + age_range + "]";
    }

    public String getImageUrl() {
        if (picture == null) {
            return null;
        }
        return picture.getUrl();
    }
}