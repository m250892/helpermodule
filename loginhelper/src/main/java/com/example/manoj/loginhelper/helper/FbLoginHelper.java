package com.example.manoj.loginhelper.helper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.manoj.loginhelper.LoginConstants;
import com.example.manoj.loginhelper.models.SignInRequestCallback;
import com.example.manoj.loginhelper.models.SignInResponse;
import com.example.manoj.loginhelper.models.fbloginresponse.FbSignInResponseModel;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestBatch;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by manoj on 07/12/15.
 */
public class FbLoginHelper {
    private Context mConstext;
    private FragmentActivity fragmentActivity;
    private static FbLoginHelper fbLoginHelper;

    private CallbackManager callbackManager;
    private AccessToken fbAccessToken;
    private static final String[] FB_PERMISSION_LIST = {"email", "user_friends", "user_about_me", "user_birthday", "user_education_history", "user_work_history"};

    private SignInRequestCallback signInRequestCallback;

    private FbLoginHelper() {
    }

    public static FbLoginHelper getInstance() {
        if (null == fbLoginHelper) {
            fbLoginHelper = new FbLoginHelper();
        }
        return fbLoginHelper;
    }

    public void setFragmentActivity(FragmentActivity fragmentActivity) {
        this.fragmentActivity = fragmentActivity;
        this.mConstext = fragmentActivity.getBaseContext();
        callbackManager = CallbackManager.Factory.create();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    public void signIn(SignInRequestCallback signInRequestCallback) {
        Log.d("manoj", "FbLoginHelper signIn called");
        this.signInRequestCallback = signInRequestCallback;
        LoginManager fbLoginManager = LoginManager.getInstance();
        fbLoginManager.registerCallback(callbackManager, fbLoginCallback);
        List<String> fbPermissions = Arrays.asList(FB_PERMISSION_LIST);
        fbLoginManager.logInWithReadPermissions(fragmentActivity, fbPermissions);
    }

    private FacebookCallback<LoginResult> fbLoginCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d("manoj", "fb login success, fbAccessToken : " + loginResult.getAccessToken());
            fbAccessToken = loginResult.getAccessToken();
            loadFbDataFromGraphApiWithBatchRequest();
        }

        @Override
        public void onCancel() {
            Log.d("manoj", "fb login cancel");
        }

        @Override
        public void onError(FacebookException error) {
            Log.d("manoj", "fb login error : " + error);
        }
    };


    private GraphRequest.GraphJSONObjectCallback profileInfoCallback = new GraphRequest.GraphJSONObjectCallback() {
        @Override
        public void onCompleted(JSONObject object, GraphResponse response) {
            Log.d("manoj", "on grapgh api response completed");
            Gson gson = new Gson();
            FbSignInResponseModel fbSignInResponseModel = gson.fromJson(object.toString(), FbSignInResponseModel.class);

            SignInResponse signInResponse = new SignInResponse();
            signInResponse.setLoginId(fbSignInResponseModel.getId());
            signInResponse.setEmail(fbSignInResponseModel.getEmail());
            signInResponse.setName(fbSignInResponseModel.getName());
            signInResponse.setGender(fbSignInResponseModel.getGender());
            signInResponse.setDob(fbSignInResponseModel.getBirthday());
            signInResponse.setLoginType(LoginConstants.FB_LOGIN);
            signInResponse.setMinAge(fbSignInResponseModel.getMinAge());
            signInResponse.setVerified(fbSignInResponseModel.getVerified());
            signInResponse.setProfileUrl(fbSignInResponseModel.getLink());
            signInResponse.setImageUrl("https://graph.facebook.com/" + fbSignInResponseModel.getId() + "/picture?type=large");

            if (signInRequestCallback != null) {
                signInRequestCallback.onSignInSuccess(LoginConstants.FB_LOGIN, signInResponse);
                signInRequestCallback = null;
            }
        }
    };

    private GraphRequest.GraphJSONArrayCallback friendListCallback = new GraphRequest.GraphJSONArrayCallback() {
        @Override
        public void onCompleted(JSONArray jsonArray, GraphResponse response) {
            // Application code for users friends
            Log.d("manoj", "Friend List \n " + jsonArray);
            Log.d("manoj", response.toString());
        }
    };

    private void loadFbDataFromGraphApiWithBatchRequest() {
        GraphRequest profileInfo = GraphRequest.newMeRequest(
                fbAccessToken, profileInfoCallback);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,birthday,gender,link,verified,picture,age_range,about,education,work,hometown");
        profileInfo.setParameters(parameters);

        GraphRequest friendList = GraphRequest.newMyFriendsRequest(fbAccessToken, friendListCallback);

        GraphRequestBatch batch = new GraphRequestBatch(profileInfo, friendList);
        batch.addCallback(new GraphRequestBatch.Callback() {
            @Override
            public void onBatchCompleted(GraphRequestBatch graphRequests) {
                // Application code for when the batch finishes
                Log.d("manoj", "OnBatch Request completed");
            }
        });
        batch.executeAsync();
    }

    public void unregisterCallback() {
        signInRequestCallback = null;
    }
}
