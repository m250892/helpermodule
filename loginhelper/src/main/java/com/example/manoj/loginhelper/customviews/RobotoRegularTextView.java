package com.example.manoj.loginhelper.customviews;

import android.content.Context;
import android.util.AttributeSet;

public class RobotoRegularTextView extends CustomTextView {

    public static final String FONT_NAME = "fonts/Roboto-Regular.ttf";

    public RobotoRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public RobotoRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public RobotoRegularTextView(Context context) {
        super(context);
    }


    @Override
    protected String getFontName() {
        return FONT_NAME;
    }
}
