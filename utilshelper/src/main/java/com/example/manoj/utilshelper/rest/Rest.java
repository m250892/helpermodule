package com.example.manoj.utilshelper.rest;

import com.example.manoj.utilshelper.Utils;

public class Rest {
    static Rest singleton = null;

    public static final String DEFAULT_TAG = "resttag";

    public Rest() {

    }

    public synchronized static Rest init() {
        if (singleton == null) {
            singleton = new Rest();
        }
        return singleton;
    }

    public synchronized static NetworkCreator connect() {
        if (singleton == null) {
            throw new IllegalArgumentException("QuickUtils not instantiated yet.");
        }
        return singleton.createRequest();
    }

    public void cancelRequests() {
        NetworkHelper.getInstance(Utils.getContext()).cancelPendingRequests(Rest.DEFAULT_TAG);
    }

    public NetworkCreator createRequest() {
        return new NetworkCreator();
    }
}
