package com.example.manoj.utilshelper.tasks;

import android.os.AsyncTask;

import com.example.manoj.utilshelper.interfaces.OnEventListener;

public abstract class CustomTask<T> extends AsyncTask<Void, Void, T> {

    protected OnEventListener callback;
    protected final Class<T> classOfT;
    protected Exception e;

    public CustomTask(Class<T> classOfT, OnEventListener callback) {
        this.callback = callback;
        this.classOfT = classOfT;
        this.e = null;
    }

    @Override
    protected T doInBackground(Void... params) {
        try {
            return doBackgroundOperation();
        } catch (Exception e) {
            this.e = e;
            return null;
        }
    }

    @Override
    protected void onPostExecute(T object) {
        if (callback != null) {
            if (e == null) {
                callback.onSuccess(object);
            } else {
                callback.onFailure(e);
            }
        }
    }

    protected void unregisterTask() {
        callback = null;
    }

    protected abstract T doBackgroundOperation();

}