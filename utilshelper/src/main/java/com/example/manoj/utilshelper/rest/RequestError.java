package com.example.manoj.utilshelper.rest;

import com.android.volley.NetworkResponse;
import com.example.manoj.utilshelper.Utils;

import java.util.Map;

public class RequestError extends Exception {
    public final static int REQUEST_RESPONSE_NOT_OK = -1;
    public final static int REQUEST_RESPONSE_OK = 200;
    public final static int REQUEST_RESPONSE_CREATED = 201;
    public final static int REQUEST_RESPONSE_ACCEPTED = 202;
    public final static int REQUEST_RESPONSE_NO_CONTENT = 204;
    public final static int REQUEST_RESPONSE_BAD_REQUEST = 400;
    public final static int REQUEST_RESPONSE_UNAUTHORIZED = 401;
    public final static int REQUEST_RESPONSE_FORBIDDEN = 403;
    public final static int REQUEST_RESPONSE_PAYMENT_REQUIRED = 402;
    public final static int REQUEST_RESPONSE_NOT_FOUND = 404;
    public final static int REQUEST_RESPONSE_GONE = 410;
    public final static int REQUEST_RESPONSE_UNPROCESSABLE_ENTITY = 422;
    public final static int REQUEST_RESPONSE_INTERNAL_SERVER_ERROR = 500;
    public final static int REQUEST_RESPONSE_SERVICE_UNAVAILABLE = 503;
    public final static int REQUEST_RESPONSE_MULTIPLE_DEVICE = 429;
    public final static int REQUEST_RESPONSE_NOT_PERMITTED = 301;
    public final static int REQUEST_RESPONSE_RESET_PASSWORD_SUCCESS = 204;
    public final static int REQUEST_RESPONSE_GSON_PARSING_FAILD = -2;

    int errorCode;
    private String errorMessage;
    Map<String, String> headers;

    RequestError(NetworkResponse response) {
        this.errorCode = response.statusCode;
        this.headers = response.headers;
        if (response.data != null)
            this.errorMessage = new String(response.data);
    }


    RequestError(String response) {
        this.errorCode = REQUEST_RESPONSE_NOT_OK;
        this.headers = null;
        this.errorMessage = response;
        Utils.log.e("ATTENTION: " + this.errorMessage);
    }

    public RequestError(Builder builder) {
        this.errorCode = builder.errorCode;
        this.errorMessage = builder.errorMessage;
        headers = null;
    }


    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public static class Builder {
        private int errorCode;
        private String errorMessage;

        public Builder setErrorCode(int errorCode) {
            this.errorCode = errorCode;
            return this;
        }

        public Builder setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
            return this;
        }

        public RequestError build() {
            return new RequestError(this);
        }
    }
}
