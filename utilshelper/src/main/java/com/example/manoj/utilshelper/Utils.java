package com.example.manoj.utilshelper;

import android.content.Context;

import com.example.manoj.utilshelper.core.DateTimeUtils;
import com.example.manoj.utilshelper.core.LogUtils;
import com.example.manoj.utilshelper.core.ScreenUtils;
import com.example.manoj.utilshelper.core.ValidateUtils;
import com.example.manoj.utilshelper.exeptions.InitNotSetException;
import com.example.manoj.utilshelper.rest.Rest;

public abstract class Utils {

    public static String TAG = "REPLACE_WITH_DESIRED_TAG";

    private static Context mContext;

    public static final int VERBOSE = android.util.Log.VERBOSE;
    public static final int DEBUG = android.util.Log.DEBUG;
    public static final int INFO = android.util.Log.INFO;
    public static final int WARN = android.util.Log.WARN;
    public static final int ERROR = android.util.Log.ERROR;

    /**
     * Developer mode for Debugging purposes
     */
    private static Boolean showLogs = null;


    /**
     * private constructor
     */
    private Utils() {
    }

    /**
     * Getter for the context
     *
     * @return the context
     */
    public static Context getContext() {

        if (mContext == null) {
            throw new InitNotSetException();
        }
        return mContext;
    }


    /**
     * Initialize QuickUtils
     */
    public static synchronized void init(Context context) {
        mContext = context;
        // Set the appropriate log TAG
        setTAG(context.getPackageName());

        Utils.rest.init();
    }

    /**
     * Set the log TAG for debug purposes
     *
     * @param tag Desired tag (e.g.: Aplication_X)
     */
    public static void setTAG(String tag) {
        TAG = tag;
    }

    /**
     * Should I show logs?
     *
     * @return true if you should
     */


    public static class log extends LogUtils {
    }

    public static class rest extends Rest {
    }

    public static class date extends DateTimeUtils {
    }

    public static class validate extends ValidateUtils {
    }

    public static class screen extends ScreenUtils {
    }


}