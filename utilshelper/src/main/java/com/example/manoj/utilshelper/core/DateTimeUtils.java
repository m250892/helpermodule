package com.example.manoj.utilshelper.core;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/***
 * Provides helper methods for date utilities.
 ***/
public class DateTimeUtils {

    public static final int YESTERDAY = -1;
    public static final int TODAY = 0;
    public static final int TOMORROW = 1;


    /**
     * Gets the current day
     *
     * @return current day
     */
    public static int getCurrentDay() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Gets the current month
     *
     * @return current month
     */
    public static int getCurrentMonth() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.MONTH);
    }

    /**
     * Gets the current year
     *
     * @return current year
     */
    public static int getCurrentYear() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.YEAR);
    }


    /**
     * Gets the current date
     *
     * @return current date
     */
    public static java.util.Date getCurrentDate() {
        return Calendar.getInstance().getTime();
    }

    /**
     * Miliseconds since midnight
     *
     * @return the number of miliseconds since midnight
     */
    public static long getTimeSinceMidnight() {
        Calendar c = Calendar.getInstance();
        long now = c.getTimeInMillis();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return now - c.getTimeInMillis();
    }

    /**
     * get Current time in milliseconds
     *
     * @return current time in milliseconds
     */
    public static long getCurrentTimeInMiliseconds() {
        return TimeUnit.MILLISECONDS.toMillis(Calendar.getInstance()
                .getTimeInMillis());
    }

    /**
     * get Current time in seconds
     *
     * @return current time in seconds
     */
    public static long getCurrentTimeInSeconds() {
        return TimeUnit.SECONDS.toSeconds(Calendar.getInstance()
                .getTimeInMillis());

    }


    /**
     * Gets a date with a desired format as a String
     *
     * @param day    Can be: <li>QuickUtils.date.YESTERDAY</li><li>
     *               QuickUtils.date.TODAY</li><li>QuickUtils.date.TOMORROW</li>
     * @param format desired format (e.g. "yyyy-MM-dd HH:mm:ss")
     * @return returns a day with the given format
     */
    public static String getDayAsString(int day, String format) {
        SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
        return simpleFormat.format(getDayAsDate(day));
    }

    /**
     * Gets a date with a desired format as a String
     *
     * @param date   date to be formated
     * @param format desired format (e.g. "yyyy-MM-dd HH:mm:ss")
     * @return returns a date with the given format
     */
    public static String formatDate(long date, String format) {
        return formatDateBase(date, format, null);
    }

    /**
     * Gets a date with a desired format as a String
     *
     * @param date     date to be formated
     * @param format   desired format (e.g. "yyyy-MM-dd HH:mm:ss")
     * @param timeZone specify the intended timezone (e.g. "GMT", "UTC", etc.)
     * @return returns a date with the given format
     */
    public static String formatDate(long date, String format,
                                    String timeZone) {
        return formatDateBase(date, format, timeZone);
    }

    private static String formatDateBase(long date, String format,
                                         String timeZone) {
        DateFormat simpleFormat = new SimpleDateFormat(format);
        if (timeZone != null) {
            simpleFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        } else {
            simpleFormat.setTimeZone(TimeZone.getDefault());
        }
        return simpleFormat.format(date);
    }

    /**
     * Gets the desired day as a Date
     *
     * @param day Can be: <li>QuickUtils.date.YESTERDAY</li><li>
     *            QuickUtils.date.TODAY</li><li>QuickUtils.date.TOMORROW</li>
     * @return returns a Date for that day
     */
    public static Date getDayAsDate(int day) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, day);
        return cal.getTime();
    }

    /**
     * Parse a data string into a real Date
     * <p/>
     * Note: (e.g. "yyyy-MM-dd HH:mm:ss")
     *
     * @param dateString date in String format
     * @param dateFormat desired format (e.g. "yyyy-MM-dd HH:mm:ss")
     * @return
     */
    public static Date parseDate(String dateString, String dateFormat) {
        Date newDate = null;
        try {
            newDate = new SimpleDateFormat(dateFormat, Locale.ENGLISH)
                    .parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }


    /***
     * Converts ISO date string to UTC timezone equivalent.
     *
     * @param dateAndTime ISO formatted time string.
     ****/
    public static String getUtcTime(String dateAndTime) {
        Date d = parseDate(dateAndTime);

        String format = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());

        // Convert Local Time to UTC
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return sdf.format(d);
    }

    /****
     * Parses date string and return a {@link java.util.Date} object
     *
     * @return The ISO formatted date object
     *****/
    public static Date parseDate(String date) {

        if (date == null) {
            return null;
        }

        StringBuffer sbDate = new StringBuffer();
        sbDate.append(date);
        String newDate = null;
        Date dateDT = null;

        try {
            newDate = sbDate.substring(0, 19).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String rDate = newDate.replace("T", " ");
        String nDate = rDate.replaceAll("-", "/");

        try {
            dateDT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault()).parse(nDate);
            // Log.v( TAG, "#parseDate dateDT: " + dateDT );
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateDT;
    }


    public static Date toLocalTime(String utcDate, SimpleDateFormat sdf) throws Exception {

        // create a new Date object using
        // the timezone of the specified city
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date localDate = sdf.parse(utcDate);

        sdf.setTimeZone(TimeZone.getDefault());
        String dateFormateInUTC = sdf.format(localDate);

        return sdf.parse(dateFormateInUTC);
    }


    public static Calendar getNow() {
        Calendar now = Calendar.getInstance();
        now.setLenient(false);
        return now;
    }

    public static int compare(Calendar time1, Calendar time2) {
        return time1.compareTo(time2);
    }

    public static boolean isSame(Calendar time1, Calendar time2) {
        return compare(time1, time2) == 0;
    }

    // Date related functions

    public static Calendar getDate() {
        Calendar date = getNow();
        clearTime(date);
        return date;
    }

    public static void setDate(Calendar date, int month, int day) {
        date.set(Calendar.MONTH, month);
        date.set(Calendar.DAY_OF_MONTH, day);
    }

    public static void setDate(Calendar date, int year, int month, int day) {
        date.set(Calendar.YEAR, year);
        setDate(date, month, day);
    }

    public static void setDate(Calendar date, Calendar newDate) {
        date.set(Calendar.ERA, newDate.get(Calendar.ERA));
        date.set(Calendar.YEAR, newDate.get(Calendar.YEAR));
        date.set(Calendar.DAY_OF_YEAR, newDate.get(Calendar.DAY_OF_YEAR));
    }

    public static void clearDate(Calendar time) {
        time.clear(Calendar.ERA);
        time.clear(Calendar.YEAR);
        time.clear(Calendar.MONTH);
        time.clear(Calendar.DAY_OF_MONTH);
    }


    public static boolean isToday(Calendar date) {
        date = (Calendar) date.clone();
        clearTime(date);
        return isSame(date, getDate());
    }


    public static Calendar getTime() {
        Calendar time = getNow();
        clearTime(time);
        return time;
    }

    public static void setTime(Calendar time, int hour, int minute, int second, int millisecond) {
        time.set(Calendar.HOUR_OF_DAY, hour);
        time.set(Calendar.MINUTE, minute);
        time.set(Calendar.SECOND, second);
        time.set(Calendar.MILLISECOND, millisecond);
    }

    public static void setTime(Calendar time, int hour, int minute, int second) {
        setTime(time, hour, minute, second, 0);
    }

    public static void setTime(Calendar time, int hour, int minute) {
        setTime(time, hour, minute, 0, 0);
    }

    public static void setTime(Calendar time, Calendar newTime) {
        setTime(time, newTime.get(Calendar.HOUR_OF_DAY),
                newTime.get(Calendar.MINUTE),
                newTime.get(Calendar.SECOND),
                newTime.get(Calendar.MILLISECOND));
    }


    public static void clearTime(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.clear(Calendar.MINUTE);
        date.clear(Calendar.SECOND);
        date.clear(Calendar.MILLISECOND);
    }


    @SuppressLint("SimpleDateFormat")
    public static String getString(Calendar time, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(time.getTime());
    }

    public static String getWeekdayString(Calendar date) {
        return getString(date, "EE");
        //return WEEKDAY_NAMES[date.get(Calendar.DAY_OF_WEEK) - 1];
    }

    public static String getHourMinuteString(Calendar time) {
        return getString(time, "HH:mm");
    }

    public static String getNormalString(Calendar time) {
        return getString(time, "yyyy-M-d HH:mm:ss:SSSS");
    }

    /**
     * Gets abbreviated name of the month from the given date.
     *
     * @param date ISO format date
     * @return Returns the name of the month
     */
    public static String getMonthAbbreviated(String date) {
        Date dateDT = parseDate(date);

        if (dateDT == null) {
            return null;
        }

        // Get current date
        Calendar c = Calendar.getInstance();
        // it is very important to
        // set the date of
        // the calendar.
        c.setTime(dateDT);
        return convertMonth(c.get(Calendar.MONTH), true);
    }

    /***
     * Gets the name of the month from the given date.
     *
     * @param date ISO format date
     * @return Returns the name of the month
     ***/
    public static String getMonth(String date) {
        Date dateDT = parseDate(date);

        if (dateDT == null) {
            return null;
        }

        // Get current date
        Calendar c = Calendar.getInstance();
        // it is very important to
        // set the date of
        // the calendar.
        c.setTime(dateDT);
        int day = c.get(Calendar.MONTH);
        return convertMonth(day, false);
    }

    /**
     * Returns abbreviated (3 letters) day of the week.
     *
     * @param date ISO format date
     * @return The name of the day of the week
     */
    public static String getDayOfWeekAbbreviated(String date) {
        Date dateDT = parseDate(date);

        if (dateDT == null) {
            return null;
        }

        // Get current date
        Calendar c = Calendar.getInstance();
        // it is very important to
        // set the date of
        // the calendar.
        c.setTime(dateDT);
        int day = c.get(Calendar.DAY_OF_WEEK);

        String dayStr = null;

        switch (day) {

            case Calendar.SUNDAY:
                dayStr = "Sun";
                break;

            case Calendar.MONDAY:
                dayStr = "Mon";
                break;

            case Calendar.TUESDAY:
                dayStr = "Tue";
                break;

            case Calendar.WEDNESDAY:
                dayStr = "Wed";
                break;

            case Calendar.THURSDAY:
                dayStr = "Thu";
                break;

            case Calendar.FRIDAY:
                dayStr = "Fri";
                break;

            case Calendar.SATURDAY:
                dayStr = "Sat";
                break;
        }

        return dayStr;
    }


    /**
     * Get number with a suffix
     *
     * @param number number that will be converted
     * @return (e.g. "1" becomes "1st", "3" becomes "3rd", etc)
     */
    public static String getNumberWithSuffix(int number) {
        int j = number % 10;
        if (j == 1 && number != 11) {
            return number + "st";
        }
        if (j == 2 && number != 12) {
            return number + "nd";
        }
        if (j == 3 && number != 13) {
            return number + "rd";
        }
        return number + "th";
    }


    /**
     * Converts a month by number to full text
     *
     * @param month    number of the month 1..12
     * @param useShort boolean that gives "Jun" instead of "June" if true
     * @return returns "January" if "1" is given
     */
    public static String convertMonth(int month, boolean useShort) {
        String monthStr;
        switch (month) {
            default:
                monthStr = "January";
                break;
            case Calendar.FEBRUARY:
                monthStr = "February";
                break;
            case Calendar.MARCH:
                monthStr = "March";
                break;
            case Calendar.APRIL:
                monthStr = "April";
                break;
            case Calendar.MAY:
                monthStr = "May";
                break;
            case Calendar.JUNE:
                monthStr = "June";
                break;
            case Calendar.JULY:
                monthStr = "July";
                break;
            case Calendar.AUGUST:
                monthStr = "August";
                break;
            case Calendar.SEPTEMBER:
                monthStr = "September";
                break;
            case Calendar.OCTOBER:
                monthStr = "October";
                break;
            case Calendar.NOVEMBER:
                monthStr = "November";
                break;
            case Calendar.DECEMBER:
                monthStr = "December";
                break;
        }
        if (useShort) monthStr = monthStr.substring(0, 3);
        return monthStr;
    }

}