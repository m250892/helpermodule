package com.example.manoj.loginhelper.models.gplusloginresponsemodel;

public class AgeRange {
    private String min;

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    @Override
    public String toString() {
        return "ClassPojo [min = " + min + "]";
    }
}
