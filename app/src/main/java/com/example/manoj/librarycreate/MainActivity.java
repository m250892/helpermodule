package com.example.manoj.librarycreate;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import com.example.manoj.loginhelper.helper.BaseLoginHelperActivity;
import com.example.manoj.utilshelper.Utils;
import com.example.manoj.utilshelper.interfaces.RequestCallback;
import com.example.manoj.utilshelper.rest.RequestError;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseLoginHelperActivity {

    @Bind(R.id.fragment_container)
    FrameLayout fragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Utils.init(this);
        ButterKnife.bind(this);
        //addFragment(LoginFragment.newInstance());

    }

    @OnClick(R.id.fragment_container)
    public void onFragmentContainerClick() {
        Utils.rest.connect()
                .GET()
                .load("https://demo9266994.mockable.io/products")
                .as(Dummy.class)
                .withCallback(new RequestCallback<Dummy>() {
                    @Override
                    public void onRequestSuccess(Dummy o) {
                        Utils.log.d("on resoponse : " + o.getData());
                    }

                    @Override
                    public void onRequestError(RequestError error) {
                        Utils.log.d("on failure : " + error.getErrorMessage());
                    }
                });
    }

    @Override
    protected void onStop() {
        super.onStop();
        ButterKnife.unbind(this);
    }

    private void addFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(com.example.manoj.loginhelper.R.id.fragment_container, fragment).commit();
    }

    class Dummy {
        private float data;

        public float getData() {
            return data;
        }
    }
}
