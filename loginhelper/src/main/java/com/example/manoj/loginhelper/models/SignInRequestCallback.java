package com.example.manoj.loginhelper.models;


/**
 * Created by manoj on 07/12/15.
 */
public interface SignInRequestCallback {

    void onSignInSuccess(String SignInMethod, SignInResponse signInResponse);

    void onSignInFailed(String SignInMethod, String message);

}
