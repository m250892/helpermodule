package com.example.manoj.utilshelper.interfaces;

import com.example.manoj.utilshelper.rest.RequestError;

public interface RequestCallback<T> {
    public void onRequestSuccess(T t);

    public void onRequestError(RequestError error);
}