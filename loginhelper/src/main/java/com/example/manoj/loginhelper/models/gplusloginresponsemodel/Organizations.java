package com.example.manoj.loginhelper.models.gplusloginresponsemodel;

public class Organizations {
    private String startDate;

    private String title;

    private String name;

    private String primary;

    private String type;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ClassPojo [startDate = " + startDate + ", title = " + title + ", name = " + name + ", primary = " + primary + ", type = " + type + "]";
    }
}