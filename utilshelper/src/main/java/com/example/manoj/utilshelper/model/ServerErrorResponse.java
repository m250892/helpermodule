package com.example.manoj.utilshelper.model;

import com.example.manoj.utilshelper.core.StringUtils;

import java.util.Collection;
import java.util.List;

public class ServerErrorResponse {

    private Object message;

    public String getMessage() {
        if (message == null) {
            return "Couldn't fetch message from responseObject";
        }
        if (message instanceof String) {
            return message.toString();
        } else if (message instanceof List) {
            return StringUtils.join((Collection<?>) message);
        } else {
            return "Couldn't fetch message from responseObject";
        }
    }

    public void setMessage(Object messageObject) {
        this.message = messageObject;
    }

}