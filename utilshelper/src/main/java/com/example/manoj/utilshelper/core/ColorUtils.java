package com.example.manoj.utilshelper.core;

public class ColorUtils {

    public enum DefaultColors {
        RED(0xffe57373),
        PINK(0xfff06292),
        PURPLE(0xffba68c8),
        DEEP_PURPLE(0xff9575cd),
        INDIGO(0xff7986cb),
        BLUE(0xff64b5f6),
        CYAN(0xff4dd0e1),
        LIGHT_BLUE(0xff4fc3f7),
        TEAL(0xff4db6ac),
        GREEN(0xff81c784),
        LIGHT_GREEN(0xffaed581),
        LIME(0xffdce775),
        YELLOW(0xfffff176),
        AMBER(0xffffd54f),
        ORANGE(0xffffb74d),
        DEEP_ORANGE(0xffff8a65),
        BROWN(0xffa1887f),
        GREY(0xffe0e0e0),
        BLUE_GREY(0xff90a4ae);

        private int color;

        DefaultColors(int color) {
            this.color = color;
        }

        public int getColor() {
            return color;
        }
    }

    private ColorUtils() {
    }

    public static int fromText(String text) {
        return DefaultColors.values()[text.hashCode() % DefaultColors.values().length].getColor();
    }

    public static String addAlpha(String originalColor, double alpha) {
        long alphaFixed = Math.round(alpha * 255);
        String alphaHex = Long.toHexString(alphaFixed);
        if (alphaHex.length() == 1) {
            alphaHex = "0" + alphaHex;
        }
        originalColor = originalColor.replace("#", "#" + alphaHex);


        return originalColor;
    }
}