package com.example.manoj.utilshelper.interfaces;

public interface OnEventListener<T> {
    public void onSuccess(T object);

    public void onFailure(Exception e);
}