package com.example.manoj.loginhelper.helper;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.manoj.loginhelper.models.IPermissionsCallback;
import com.example.manoj.loginhelper.models.SignInRequestCallback;
import com.example.manoj.loginhelper.models.SignInResponse;

import org.json.JSONObject;

/**
 * Created by manoj on 13/02/16.
 */
public class BaseFbAndGPlusLoginHelperFragment extends Fragment implements SignInRequestCallback {

    public void doLoginWithFb() {
        Log.d("manoj", "Fb login button click");
        FbLoginHelper.getInstance().signIn(this);
    }

    public void doLoginWithGPlus() {
        Log.d("manoj", "google login button click");

        ((BaseLoginHelperActivity) getActivity()).requestPermission(IPermissionsCallback.GET_ACCOUNTS, new IPermissionsCallback() {
            @Override
            public void onPermissionRequestResult(int requestCode, boolean result) {
                if (result) {
                    GPlusLoginHelper.getInstance().signIn(BaseFbAndGPlusLoginHelperFragment.this);
                } else {
                    showToastMessage("Please provide permission for login via google!!");
                }
            }
        });
    }


    public boolean validatePassword(String password) {
        return password.length() > 5;
    }

    public void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void showToastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        FbLoginHelper.getInstance().unregisterCallback();
        GPlusLoginHelper.getInstance().unregisterCallback();
        super.onDestroyView();
    }

    @Override
    public void onSignInSuccess(String signInMethod, SignInResponse signInResponse) {
        Log.d("manoj", "Login Success with : " + signInMethod);
        Log.d("manoj", "Sign data : " + signInResponse.toString());

        JSONObject jsonObject = signInResponse.getJsonObject();
        if (jsonObject != null) {
            Log.d("manoj", "Json object data : " + jsonObject);
        }
    }

    @Override
    public void onSignInFailed(String signInMethod, String message) {
        Log.d("manoj", "Login failed with : " + signInMethod + ", message: " + message);

    }
}
